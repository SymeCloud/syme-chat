#!/bin/bash

export CMAKE_ARGS="-DLLAMA_BLAS=ON -DLLAMA_BLAS_VENDOR=OpenBLAS"
export FORCE_CMAKE=1

pip3 install -r "$@"
