# syme-chat

## Introduction

SymeChat is a chatbot that can talk about anything you want. It's designed for CPU-only for lower-cost LLM solution.

## Deployment

It's recommended to deploy with **Alexon** ([alexon.dev](alexon.dev)).

```bash
alexon app create syme-chat.yaml
```

## Llama2 GGUF format

llama.cpp replaced GGML with GGUF now. And SymeChat follows the upstream to only support GGUF. If you don't have your prefer Llama2 quantized model, please try [SymeCloud/Llama2-7b-Chat-GGUF](https://huggingface.co/SymeCloud/Llama2-7b-Chat-GGUF).

## Use policy

You must abide by the Llama2 use policy (https://ai.meta.com/llama/use-policy/).

## Credits

- **Llama2-7B** (https://about.fb.com/news/2023/07/llama-2/)
- **llama.cpp** (https://github.com/ggerganov/llama.cpp)
- **llama2-webui** (https://github.com/liltom-eth/llama2-webui)

## Copyright

**SymeCloud Limited** ([syme.dev](syme.dev)).
