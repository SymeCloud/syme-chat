FROM python:3.11
COPY . syme-chat
RUN rm -fr syme-chat/.git syme-chat/__pycache__ syme-chat/gradio_cached_examples
RUN rm -fr syme-chat/*~
RUN pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple -U pip
RUN pip3 config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
RUN pip3 install gradio sentencepiece
RUN CMAKE_ARGS="-DLLAMA_BLAS=ON -DLLAMA_BLAS_VENDOR=OpenBLAS" FORCE_CMAKE=1 pip3 install llama-cpp-python
